# Extract images from video

Extract the images from a video in python utilizing OpenCV

# Flags:
- --file/-f : Specify the video file to use as the source
- --device/-d : Specify the index of a video device to use as the source, such as a webcam
- --steps/-s : Specify the amount of frames to skip in between when it saves a frame

# Usage:
```
python3 video.py --file test.mp4 -s 100
python3 video.py --device 0 -s 24
```