import cv2 as cv
import os, argparse, Exceptions

parser = argparse.ArgumentParser()

# Argument for File input
parser.add_argument("-f", "--file",   type=str, 
    help="Select the video file to grab images from")
# Argument for Device input 
parser.add_argument("-d", "--device", type=int, 
    help="Select the index of the device to capture video from, usually 0")
# Argument for how many frames to skip between captures
parser.add_argument("-s", "--steps",  type=int, 
    help="Amount of frames to skip in the video, to low and you will get lots of photos")

args = parser.parse_args()

# If no device or file is specified then it will exit instantly
if (args.file == None and args.device == None):
    raise Exceptions.InputException("Error you must use either --file or --device")

# If utilizing a file then set video to the file if not then to the device
# Arguments are set to None by default, using if(args.file) will work but 
# if(args.device) will fail if device is 0 since that is returned as false
if(args.file != None):
    video = cv.VideoCapture(args.file)
elif(args.device != None):
    video = cv.VideoCapture(args.device)

# Amount of frames to skip before saving the photos
if(args.steps):
    everyother = args.steps
else:
    everyother = 1000

directory = "Photos/"
if not os.path.exists(directory):
    os.makedirs(directory, exist_ok=True)

count = 0
while video.isOpened():
    success, image = video.read()
    if success:
        if(count % everyother == 0):
            cv.imwrite(f"{directory}/Frame{count}.png", image)
        count += 1
    else:
        break

cv.destroyAllWindows()
video.release()